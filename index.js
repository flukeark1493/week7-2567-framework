const express = require('express')
const app = express()
require('dotenv').config()

const PORT = process.env.PORT || 3300

//localhost:3000
app.get('/', function (req,res){
    return res.status(200).json({ message: "GET request to Home page"})
})
//localhost:3000/admin
app.post('/admin', function (req,res){
    return res.status(201).json({ message: "POST request to Home page"})
})
//localhost:3000/service
app.put('/service', function (req,res){
    return res.status(201).json({ message: "PUT request to Service page"})
})
//localhost:3000/contact
app.delete('/contact', function (req,res){
    return res.status(201).json({ message: "DELETE request to Contact page"})
})

app.listen(PORT, () => {

    console.log(`Server running at http://localhost:${PORT}`);

})
